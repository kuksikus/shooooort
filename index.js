const http = require('http');
const url = require('url');
const fs = require('fs');

const {shorten, stats} = require('./src/routes');

const port = 3000;

/**
 * Returns path or index.html if root path
 * @param {String} path Path from request
 * @returns {String} Prepared path
 */
const getPreparedPathName = (path) => {
	return path === '/'
		? './dist/index.html'
		: `.${path}`;
};

const requestHandler = (request, response) => {
	const parsedUrl = url.parse(request.url, true);
	const {pathname, query} = parsedUrl;
	const filePathname = getPreparedPathName(parsedUrl.pathname);

	switch (pathname) {
		case '/shorten':
			shorten(query, response);
			return;

		case '/stats':
			stats(query, response);
			return;
	}

	fs.exists(filePathname, (exist) => {
		if (!exist) {
			response.statusCode = 404;
			response.end();
			return;
		}

		fs.readFile(filePathname, (err, data) => {
			if (err) {
				response.statusCode = 500;
				response.end();
				return;
			}

			response.end(data);
		});
	});

	/*
	 * Response.setHeader('Content-Type', 'text/html; charset=utf-8');
	 * response.end('endpoint');
	 */
};

const server = http.createServer(requestHandler);

server.listen(port, (error) => {
	if (error) {
		throw new Error(error);
	}

	console.log(`http://localhost:${port}`);
});
