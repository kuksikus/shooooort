const path = require('path');
const GoogleFontsPlugin = require('google-fonts-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactRootPlugin = require('html-webpack-react-root-plugin');

module.exports = {
	entry: './src/index.js',
	output: {
		path: `${__dirname}/dist`,
		publicPath: 'dist/',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				loader: 'babel-loader',
				include: [
					path.resolve(__dirname, 'src')
				],
				test: /\.jsx?$/,
				query: {
					presets: ['es2015', 'stage-2', 'react']
				}
			}
		]
	},
	plugins: [
		new GoogleFontsPlugin({
			fonts: [
				{
					family: 'Montserrat'
				},
				{
					family: 'Roboto',
					variants: ['300', '600']
				}
			],
			local: false
		}),
		new HtmlWebpackPlugin({
			title: 'shooooort',
			inject: true
		}),
		new ReactRootPlugin()
	]
};
