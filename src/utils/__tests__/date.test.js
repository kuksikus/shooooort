import {getRelativeDate, getFormatedDate} from '..';

const date = new Date();
const featureDate = new Date(date.setDate(date.getDate() - 1));
const dateForFormat = new Date(1528555905024);

describe('Date utils', () => {
	test('getRelativeDate() should return right date', () => {
		expect(getRelativeDate(featureDate)).toBe('1 days ago');
	});

	test('getLinksFromStorage() should returns links from storage', () => {
		expect(getFormatedDate(dateForFormat)).toBe('09.06.2018 at 17:51');
	});
});
