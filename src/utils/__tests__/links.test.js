import {copyToClipboard, getLinksFromStorage} from '../links';

global.localStorage = {
	getItem: jest.fn(),
	setItem: jest.fn()
};

describe('Links utils', () => {
	test('copyToClipboard() should copy to clipboard', () => {
		const value = 'http://google.com/';

		document.execCommand = jest.fn();
		copyToClipboard(value);

		expect(document.execCommand).toBeCalledWith('copy');
	});

	test('getLinksFromStorage() should returns links from storage', () => {
		const links = [
			{
				code: 'someCode',
				url: 'http://google.com/'
			}
		];

		localStorage.setItem('links', links);
		getLinksFromStorage();
		expect(localStorage.getItem).toBeCalledWith('links');
	});
});
