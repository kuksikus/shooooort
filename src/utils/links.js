/**
 * Copies value to clipboard
 * @param {String} value String to copy
 * @returns {Boolean} Result of copy
 */
const copyToClipboard = (value) => {
	if (!value) {
		return false;
	}

	const textField = document.createElement('textarea');

	textField.innerText = value;
	document.body.appendChild(textField);
	textField.select();
	document.execCommand('copy');
	textField.remove();
	return true;
};

/**
 * Get links from storage
 * @returns {Object[]} List of links
 */
const getLinksFromStorage = () => {
	const item = localStorage.getItem('links');

	return item
		? JSON.parse(item)
		: [];
};

export {
	copyToClipboard,
	getLinksFromStorage
};
