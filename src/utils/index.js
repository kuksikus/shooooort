import {copyToClipboard, getLinksFromStorage} from './links';
import {getRelativeDate, getFormatedDate} from './date';

export {
	copyToClipboard,
	getLinksFromStorage,
	getRelativeDate,
	getFormatedDate
};
