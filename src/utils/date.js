import {
	differenceInSeconds,
	differenceInMinutes,
	differenceInHours,
	differenceInCalendarDays,
	format
} from 'date-fns';

/**
 * Get relative date
 * @param {String} date Date string
 * @returns {String} Relative date/time
 */
const getRelativeDate = (date) => {
	const currentDate = new Date();

	if (!date) {
		return null;
	}

	let dateString = '';
	const diffPeriods = {
		days: differenceInCalendarDays,
		hours: differenceInHours,
		minutes: differenceInMinutes,
		seconds: differenceInSeconds
	};

	Object.keys(diffPeriods).some((period) => {
		const diff = diffPeriods[period](currentDate, date);

		dateString = `${diff} ${period} ago`;
		return diff > 0;
	});

	return dateString;
};

/**
 * Format date
 * @param {String} date Date string
 * @returns {String} Formatted string
 */
const getFormatedDate = (date) => {
	return `${format(date, 'DD.MM.YYYY')} at ${format(date, 'HH:mm')}`;
};

export {
	getRelativeDate,
	getFormatedDate
};
