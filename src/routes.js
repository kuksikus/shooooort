const {shortenLink, getLinkStats} = require('./api');

const shorten = (query, response) => {
	return shortenLink(query.url)
		.then((shortenResponse) => {
			response.end(JSON.stringify(shortenResponse));
		})
		.catch((error) => {
			if (!error.response) {
				response.statusCode = 500;
				response.end();
				return;
			}

			const {response: {status, statusText}} = error;

			response.statusCode = status;
			response.end(JSON.stringify(statusText));
		});
};

const stats = (query, response) => {
	return getLinkStats(query.shortcode)
		.then((statsResponse) => {
			response.end(JSON.stringify(statsResponse));
		})
		.catch((error) => {
			if (!error.response) {
				response.statusCode = 500;
				response.end();
				return;
			}

			const {response: {status, statusText}} = error;

			response.statusCode = status;
			response.end(JSON.stringify(statusText));
		});
};

module.exports = {
	shorten,
	stats
};
