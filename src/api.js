const axios = require('axios');

axios.defaults.headers.post['Content-Type'] = 'application/json';

const API_URL = 'https://impraise-shorty.herokuapp.com';

/**
 * Request to shorten link
 * @param {String} url URL
 * @returns {Promise} Result of the request
 */
const shortenLink = (url) => {
	return axios
		.post(`${API_URL}/shorten`, {
			url
		})
		.then((response) => {
			const {data: {shortcode}} = response;

			return {
				code: shortcode
			};
		});
};

/**
 * Request to get link stats
 * @param {String} shortcode Shortcode
 * @returns {Promise} Result of the request
 */
const getLinkStats = (shortcode) => {
	if (!shortcode) {
		return Promise.reject(new Error('No shortcode'));
	}

	return axios
		.get(`${API_URL}/${shortcode}/stats`)
		.then((response) => {
			const {data} = response;

			return data;
		});
};

module.exports = {
	apiUrl: API_URL,
	shortenLink,
	getLinkStats
};
