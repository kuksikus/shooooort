import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import {getLinksFromStorage} from './utils';
import reducer from './reducers';
import App from './containers/App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const preloadedState = {
	links: getLinksFromStorage()
};

const store = createStore(
	reducer,
	preloadedState,
	composeEnhancers(applyMiddleware(thunk))
);

const Shortener = () => (
	<Provider store={store}>
		<App store={store} />
	</Provider>
);

ReactDOM.render(<Shortener/>, document.getElementById('root'));
