const links = (state = [], action) => {
	switch (action.type) {
		case 'SHORTEN_SUCCESS':
			return [
				{
					...action.link,
					isLastAdded: true
				},
				...state.map((link) => {
					link.isLastAdded = false;
					return link;
				})
			];

		case 'LINKS_UPDATE':
			return [
				...action.links
			];

		case 'CLEAR':
			return [];

		default:
			return state;
	}
};

export default links;
