const initialState = {
	isValidUrl: false,
	isActiveForm: true,
	inputValue: '',
	forceUpdated: false,
	status: ''
};

/**
 * URL validation
 * @param {String} url URL
 * @returns {Boolean} Is valid url
 */
const isValidUrl = (url) => {
	try {
		new URL(url);

		return true;
	} catch (exception) {
		return false;
	}
};


const shortener = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE_VALUE':
			return {
				...state,
				isValidUrl: isValidUrl(action.value),
				inputValue: action.value,
				forceUpdated: action.force
			};
		case 'IN_PROCESS':
			return {
				...state,
				isActiveForm: !action.inProcess
			};
		case 'STATUS':
			return {
				...state,
				status: action.status,
				statusText: action.statusText
			};
		default:
			return state;
	}
};

export default shortener;
