import {combineReducers} from 'redux';
import shortener from './shortener';
import links from './links';

export default combineReducers({
	shortener,
	links
});
