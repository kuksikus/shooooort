import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import {links as linksActions} from '../actions';
import Header from '../components/Header';
import Shortener from '../components/Shortener';
import History from '../components/History';

const Page = styled.div`
	width: 620px;
	margin: 100px auto;
`;

class MainContainer extends Component {
	constructor(props) {
		super(props);

		this._store = this.props.store;

		this.shortenLink = this.shortenLink.bind(this);
		this.clearHistory = this.clearHistory.bind(this);
	}

	shortenLink() {
		const {shortener: {inputValue}} = this.props;

		this._store.dispatch(linksActions.sendLink(inputValue));
	}

	clearHistory() {
		this._store.dispatch(linksActions.clear());
	}

	componentDidMount() {
		const {links} = this.props;

		this._store.dispatch(linksActions.updateLinksInfo(links));
	}

	render() {
		const {shortener, links, store} = this.props;

		return (
			<Page>
				<Header />
				<Shortener
					store={store}
					shortener={shortener}
					shortenLink={this.shortenLink}
				/>
				<History
					store={store}
					links={links}
					clearHistory={this.clearHistory}
				/>
			</Page>
		);
	}
}

MainContainer.propTypes = {
	shortener: PropTypes.shape({
		inputValue: PropTypes.string
	}),
	links: PropTypes.array.isRequired,
	store: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
	return {
		shortener: state.shortener,
		links: state.links
	};
};

export default connect(mapStateToProps)(MainContainer);
