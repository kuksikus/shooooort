import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LinksTableLink from './LinksTableLink';
import LinksTableVisits from './LinksTableVisits';
import LinksTableVisited from './LinksTableVisited';

const StyledTable = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

const cellStyle = () => {
	return `
		box-sizing: border-box;
		flex-grow: 1;
		padding: 0 0 13px 0;
	`;
};

const StyledHead = styled.div`
	text-transform: uppercase;
	font-family: 'Roboto';
	font-size: 14px;
	color: #AAA;
`;

const StyledCellLink = styled.div`
	${cellStyle()}
	width: 73%;
`;

const StyledCellVisits = styled.div`
	${cellStyle()}
	width: 10%;
	text-align: right;
	padding-right: 20px;
	box-sizing: border-box;
`;

const StyledCellVisited = styled.div`
	${cellStyle()}
	width: 17%;
	text-align: center;
`;

const LinksTable = (props) => {
	const getLinks = () => {
		const {links} = props;

		return links.map(({
			code,
			redirectCount,
			lastSeenDate,
			url,
			isLastAdded
		}, index) => (
			<React.Fragment key={index}>
				<LinksTableLink
					code={code}
					url={url}
					isLastAdded={isLastAdded}
				/>
				<LinksTableVisits
					redirectCount={redirectCount}
				/>
				<LinksTableVisited
					lastSeenDate={lastSeenDate}
				/>
			</React.Fragment>
		));
	};

	const linksRows = getLinks();

	return (
		<StyledTable>
				<StyledCellLink>
					<StyledHead>Link</StyledHead>
				</StyledCellLink>
				<StyledCellVisits>
					<StyledHead>Visits</StyledHead>
				</StyledCellVisits>
				<StyledCellVisited>
					<StyledHead>Last visited</StyledHead>
				</StyledCellVisited>
			{linksRows}
		</StyledTable>
	);
};

LinksTable.propTypes = {
	links: PropTypes.array.isRequired
};

export default LinksTable;
