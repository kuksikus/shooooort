import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledTitle = styled.div`
	display: inline-block;
	margin-right: 25px;
	margin-bottom: 36px;
	font-family: 'Roboto';
	font-weight: 300;
	font-size: 22px;
	color: #555;
`;

const StyledClear = styled.button`
	display: inline-block;
	font-family: 'Roboto';
	font-weight: 300;
	font-size: 16px;
	color: #EB4A42;
	border: 0;
	background: none;
	padding: 0;
	cursor: pointer;
`;

const Header = (props) => (
	<div>
		<StyledTitle>Previously shortened by you</StyledTitle>
		<StyledClear onClick={props.clearHistory}>Clear history</StyledClear>
	</div>
);

Header.propTypes = {
	clearHistory: PropTypes.func.isRequired
};

export default Header;
