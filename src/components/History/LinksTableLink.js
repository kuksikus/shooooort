import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import {apiUrl} from '../../api';
import {copyToClipboard} from '../../utils';

const TIME_TO_HIDE = 0.5;

const lastAddedStyle = `
	&::before {
		content: '';
		display: block;
		position: absolute;
		left: -22px;
		top: 4px;
		width: 4px;
		height: 80px;
		background-color: red;
	}
`;

const StyledCellLink = styled.div`
	box-sizing: border-box;
	flex-grow: 1;
	padding-top: 18px;
	margin-bottom: 8px;
	width: 73%;
	position:relative;

	${({isLastAdded}) => isLastAdded ? lastAddedStyle : null}
`;

const StyledLinkDomain = styled.span`
	color: #555;
`;

const StyledLinkCode = styled.span`
	color: #EB4A42;
`;

const StyledUrl = styled.span`
	color: #D0CFCF;
	line-height: 36px;
`;

const StyledFullLinkWrapper = styled.div`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	color: #D0CFCF;
`;

const StyledLink = styled.a`
	display: inline-block;
	font-family: 'Roboto';
	font-weight: 600;
	font-size: 16px;
	text-decoration: none;
`;

const StyledCopy = styled.div`
	display: inline-block;
	margin-left: 24px;
	font-family: 'Roboto';
	color: #EB4A42;
	cursor: pointer;

	opacity: ${({isCopied}) => isCopied ? 0 : 1};
	transition: opacity 500ms ease-out;
`;

class LinksTableLink extends React.Component {
	constructor(props) {
		super(props);
		const {code} = props;

		this.state = {
			isHovered: false,
			isCopied: false
		};

		this.shortLink = `${apiUrl}/${code}`;

		this.onClickCopy = this.onClickCopy.bind(this);
	}

	hover(isHovered) {
		this.setState({
			isHovered
		});
	}

	onClickCopy() {
		copyToClipboard(this.shortLink);

		this.setState({
			isCopied: true
		});

		window.setTimeout(() => {
			this.setState({
				isCopied: false,
				isHovered: false
			});
		}, TIME_TO_HIDE * 1000);
	}

	render() {
		const {code, url, isLastAdded} = this.props;
		const {isHovered, isCopied} = this.state;

		const linkText = isCopied
			? 'Copied!'
			: 'Click to copy this link';

		const copyLink = isHovered
		? (
			<StyledCopy
				onClick={this.onClickCopy}
				isCopied={isCopied}
			>
				{linkText}
			</StyledCopy>
		)
		: '';

		return (
			<StyledCellLink
				isLastAdded={isLastAdded}
				onMouseEnter={this.hover.bind(this, true)}
				onMouseLeave={this.hover.bind(this, false)}
			>
				<div>
					<StyledLink
						href={this.shortLink}
						target="_blank"
					>
						<StyledLinkDomain>shooooort.com/</StyledLinkDomain>
						<StyledLinkCode>{code}</StyledLinkCode>
					</StyledLink>
					{copyLink}
				</div>
				<StyledFullLinkWrapper>
					<StyledLink href={url} target="_blank">
						<StyledUrl>
							{url}
						</StyledUrl>
					</StyledLink>
				</StyledFullLinkWrapper>
			</StyledCellLink>
		);
	}
}

LinksTableLink.propTypes = {
	code: PropTypes.string.isRequired,
	url: PropTypes.string.isRequired,
	isLastAdded: PropTypes.bool
};

export default LinksTableLink;
