import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledVisits = styled.div`
	font-family: 'Roboto';
	line-height: 55px;
	color: #555;
`;

const StyledCellVisits = styled.div`
	box-sizing: border-box;
	flex-grow: 1;
	padding-top: 18px;
	margin-bottom: 8px;
	width: 10%;
	text-align: right;
	padding-right: 20px;
	box-sizing: border-box;
`;

const LinksTableVisits = ({redirectCount}) => (
	<StyledCellVisits>
		<StyledVisits>
			{redirectCount}
		</StyledVisits>
	</StyledCellVisits>
);

LinksTableVisits.propTypes = {
	redirectCount: PropTypes.number
};

LinksTableVisits.defaultProps = {
	redirectCount: 0
};

export default LinksTableVisits;
