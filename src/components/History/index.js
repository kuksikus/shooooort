import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import LinksTable from './LinksTable';

const History = (props) => {
	const {links, clearHistory} = props;

	const table = (
		<div>
			<Header clearHistory={clearHistory} />
			<LinksTable links={links} />
		</div>
	);

	return links.length
		? table
		: '';
};

History.propTypes = {
	links: PropTypes.array.isRequired,
	clearHistory: PropTypes.func.isRequired
};

export default History;
