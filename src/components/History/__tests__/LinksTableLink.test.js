import React from 'react';
import {shallow} from 'enzyme';
import {copyToClipboard} from '../../../utils';

import LinksTableLink from '../LinksTableLink';

jest.mock('../../../utils');

copyToClipboard
	.mockReturnValue(true);

const getComponent = () => {
	return shallow(
		<LinksTableLink
			code={'genCode'}
			url={'http://google.com/'}
			isLastAdded={false}
		/>
	);
};

describe('LinksTableLink', () => {
	test('Link hover should change the state', () => {
		const component = getComponent();

		component.instance().hover(true);
		expect(component.state('isHovered')).toBe(true);

		component.instance().hover(false);
		expect(component.state('isHovered')).toBe(false);
	});

	test('Link click should copy short link', () => {
		const component = getComponent();

		component.instance().onClickCopy();

		expect(copyToClipboard).toBeCalled();
		expect(component.state('isCopied')).toBe(true);
	});
});
