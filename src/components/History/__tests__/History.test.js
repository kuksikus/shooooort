import React from 'react';
import {create} from 'react-test-renderer';
import {format, differenceInSeconds} from 'date-fns';

import History from '../';

const clearHistory = () => {};

const getComponent = () => {
	const links = [
		{
			code: 'gshl',
			url: 'http://google.com/',
			lastSeenDate: 'Tue May 27 2018 17:18:25 GMT+0300',
			redirectCount: 2
		}
	];

	return create(
		<History clearHistory={clearHistory} links={links} />
	);
};

const constantDate = new Date('Tue May 27 2018 17:18:25 GMT+0300');

Date = class extends Date {
	constructor() {
		super();
		return constantDate;
	}
};

jest.mock('date-fns');
format
	.mockReturnValueOnce('27.05.2018')
	.mockReturnValueOnce('17:18');

differenceInSeconds
	.mockReturnValue('2 days');


describe('History', () => {
	test('History rendered correctly', () => {
		const component = getComponent();

		expect(component.toJSON()).toMatchSnapshot();
	});

	test('History rendered without links', () => {
		const component = create(
			<History clearHistory={clearHistory} links={[]} />
		);

		expect(component.toJSON()).toMatchSnapshot();
	});
});
