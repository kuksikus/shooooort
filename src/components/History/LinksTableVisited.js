import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {getRelativeDate, getFormatedDate} from '../../utils';

const StyledLastVisited = styled.div`
	font-family: 'Roboto';
	line-height: 55px;
	color: #555;
`;

const StyledCellVisited = styled.div`
	box-sizing: border-box;
	flex-grow: 1;
	padding-top: 18px;
	margin-bottom: 8px;
	width: 17%;
	height: 75px;
	overflow: hidden;
	text-align: center;
`;

const LinksTableVisited = ({lastSeenDate}) => (
	<StyledCellVisited>
		<StyledLastVisited title={getFormatedDate(lastSeenDate)}>
			{getRelativeDate(lastSeenDate)}
		</StyledLastVisited>
	</StyledCellVisited>
);

LinksTableVisited.propTypes = {
	lastSeenDate: PropTypes.string
};

export default LinksTableVisited;
