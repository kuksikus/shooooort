import React from 'react';
import styled from 'styled-components';

import Logo from './Logo';

const StyledHeader = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: flex-end;
	margin-bottom: 50px;
`;

const Description = styled.div`
	font-family: 'Roboto';
	font-weight: 300;
	font-size: 16px;
	color: #AAA;
	padding-bottom: 10px;
`;

const Header = () => (
	<StyledHeader>
		<Logo />
		<Description>
			The link shortener with a long name
		</Description>
	</StyledHeader>
);

export default Header;
