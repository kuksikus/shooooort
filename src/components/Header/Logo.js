import React from 'react';
import styled from 'styled-components';

const StyledLogo = styled.a`
	font-family: Montserrat;
	font-size: 47px;
	font-weight: bold;
	color: #EB4A42;
`;

const Logo = () => (
	<StyledLogo href="/">Shooooort</StyledLogo>
);

export default Logo;
