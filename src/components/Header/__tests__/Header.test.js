import React from 'react';
import Header from '../';
import {create} from 'react-test-renderer';

describe('Header', () => {
	test('Header rendered correctly', () => {
		const component = create(<Header />);

		expect(component.toJSON()).toMatchSnapshot();
	});
});
