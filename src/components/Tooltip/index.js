import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledTooltip = styled.div`
	position: absolute;
	display: inline-block;
	top: 52px;
	left: 15px;
	padding: 0 5px 2px;
	background-color: ${({theme}) => theme === 'error' ? '#EB4A42' : '#EAEAEA'};
	color: ${({theme}) => theme === 'error' ? '#FFF' : '#555'};
	border-radius: 2px;
	opacity: ${({isHidden}) => isHidden ? 0 : 1};
	transition: opacity 300ms ease-out;

	&::after {
		content: '';
		position: absolute;
		top: -5px;
		left: 10px;
		transform: rotate(45deg);
		display: block;
		width: 10px;
		height: 10px;
		background-color: ${
			({theme}) => theme === 'error' ? '#EB4A42' : '#EAEAEA'
		};
	}
`;

class Tooltip extends React.PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			isHidden: false
		};

		this.timeout = null;

		this.hide = this.hide.bind(this);
	}

	hide() {
		this.setState({
			isHidden: true
		});
	}

	componentWillUnmount() {
		this.timeout && window.clearTimeout(this.timeout);
	}

	render() {
		const {theme} = this.props;
		const {isHidden} = this.state;

		this.timeout = window.setTimeout(this.hide, 1200);

		return (
			<StyledTooltip
				theme={theme}
				isHidden={isHidden}
			>
				{this.props.text}
			</StyledTooltip>
		);
	}
}

Tooltip.propTypes = {
	text: PropTypes.string.isRequired,
	theme: PropTypes.string
};

export default Tooltip;
