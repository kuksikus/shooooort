import React from 'react';
import Tooltip from '../';
import {create} from 'react-test-renderer';
import {shallow} from 'enzyme';

describe('Tooltip', () => {
	test('Tooltip rendered correctly', () => {
		const component = create(<Tooltip text={'Tooltip text'} />);

		expect(component.toJSON()).toMatchSnapshot();
	});

	test('Tooltip hide should change the state', () => {
		const component = shallow(<Tooltip text={'Tooltip text'} />);

		component.instance().hide();
		expect(component.state('isHidden')).toBe(true);
	});
});
