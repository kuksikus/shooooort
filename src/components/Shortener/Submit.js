import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledSubmit = styled.input`
	width: 100%;
	height: 42px;
	background-color: ${({isActive}) => isActive ? '#EB4A42' : '#E0E0E0'};
	padding: 0;
	border: 0;
	border-radius: 3px;
	line-height: 42px;
	font-family: 'Roboto';
	font-weight: 300;
	font-size: 16px;
	cursor: ${({isActive}) => isActive ? 'pointer' : 'normal'};
	color:  ${({isActive}) => isActive ? '#FFF' : '#BFBFBF'};
	transition: background-color 300ms ease-out;
	outline: 0;

	:focus {
		box-shadow: 0 0 1px 1px #CCC;
	}
`;

const Submit = (props) => {
	const {shortener: {isValidUrl, isActiveForm}} = props;
	const isActiveButton = isValidUrl && isActiveForm;

	return (
		<StyledSubmit
			isActive={isActiveButton}
			type="submit"
			value="Shorten this link"
			disabled={!isActiveButton}
		/>
	);
};

Submit.propTypes = {
	shortener: PropTypes.shape({
		isValidUrl: PropTypes.bool.isRequired,
		isActiveForm: PropTypes.bool.isRequired
	})
};

export default Submit;
