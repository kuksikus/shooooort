import React from 'react';
import {create} from 'react-test-renderer';
import {shallow} from 'enzyme';

import Input from '../Input';

const fakeStore = {
	dispatch: jest.fn()
};

const fakeEvent = {
	target: {
		value: 'someValue'
	}
};

const changeValue = jest.fn();

const getProps = () => {
	return {
		store: fakeStore,
		shortener: {
			isActiveForm: true,
			inputValue: '',
			status: ''
		},
		changeValue
	};
};

const getComponent = (props = getProps()) => {
	return <Input {...props} />;
};

describe('Shortener Input', () => {
	test('Input rendered correctly', () => {
		const component = getComponent();

		expect(create(component).toJSON()).toMatchSnapshot();
	});

	test('Input onChange should call changeValue', () => {
		const component = getComponent();

		shallow(component)
			.instance()
			.onChange(fakeEvent);

		expect(changeValue).toBeCalledWith(fakeEvent.target.value);
	});

	test('Input getTooltip should return empty', () => {
		const component = getComponent();

		const tooltip = shallow(component)
			.instance()
			.getTooltip();

		expect(tooltip).toBe('');
	});
});
