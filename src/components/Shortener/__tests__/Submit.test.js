import React from 'react';
import {create} from 'react-test-renderer';

import Submit from '../Submit';

describe('Shortener Submit', () => {
	test('Submit rendered correctly', () => {
		const props = {
			shortener: {
				isActiveForm: true,
				isValidUrl: true
			}
		};
		const component = <Submit {...props} />;

		expect(create(component).toJSON()).toMatchSnapshot();
	});

	test('Submit rendered disabled correctly', () => {
		const props = {
			shortener: {
				isActiveForm: false,
				isValidUrl: true
			}
		};
		const component = <Submit {...props} />;

		expect(create(component).toJSON()).toMatchSnapshot();
	});
});
