import React from 'react';
import {shallow} from 'enzyme';
import {create} from 'react-test-renderer';

import {shortener as shortenerActions} from '../../../actions';
import Shortener from '../';

const fakeStore = {
	dispatch: jest.fn()
};

const shortenLink = jest.fn();

const fakeEvent = {
	preventDefault: jest.fn()
};

const getComponent = () => {
	const props = {
		store: fakeStore,
		shortener: {
			isValidUrl: false,
			isActiveForm: false,
			inputValue: '',
			status: ''
		},
		shortenLink
	};

	return <Shortener {...props} />;
};

describe('Shortener', () => {
	test('Shortener rendered correctly', () => {
		const component = getComponent();

		expect(create(component).toJSON()).toMatchSnapshot();
	});

	test('Changing value should dispatch action', () => {
		const component = getComponent();
		const value = 'http://google.com';

		shallow(component)
			.instance()
			.changeValue(value);

		expect(fakeStore.dispatch)
			.toBeCalledWith(shortenerActions.changeValue(value));
	});

	test('Form submitting should shorten link', () => {
		const component = getComponent();

		shallow(component)
			.instance()
			.onSubmit(fakeEvent);

		expect(shortenLink).toBeCalled();
		expect(fakeEvent.preventDefault).toBeCalled();
	});
});
