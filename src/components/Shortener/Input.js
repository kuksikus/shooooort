import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Tooltip from '../Tooltip';

const StyledInput = styled.input`
	width: 100%;
	height: 42px;
	padding: 0 18px;
	background-color: #EAEAEA;
	border: 0;
	border-radius: 3px;
	line-height: 42px;
	box-sizing: border-box;
	color: #555;
	font-size: 16px;
	outline: 0;

	&::placeholder {
		color: #CCC;
	}

	&:disabled {
		color: #CCC;
		transition: color 300ms ease-out;
	}
`;

const StyledWrapper = styled.div`
	position: relative;
	font-family: 'Roboto';
	font-weight: 300;
`;

class Input extends React.Component {
	constructor(props) {
		super(props);

		this._input = React.createRef();

		this.onChange = this.onChange.bind(this);
	}

	onChange(event) {
		this.props.changeValue(event.target.value);
	}

	getTooltip() {
		const {shortener: {status, statusText}} = this.props;

		switch (status) {
			case 'success':
				return (
					<Tooltip
						text={statusText || 'Link copied to clipboard'}
					/>
				);
			case 'error':
				return (
					<Tooltip
						text={
							statusText ||
							'An error occured. Please try again later'
						}
						theme={'error'}
					/>
				);
			default:
				return '';
		}
	}

	componentDidUpdate() {
		const {shortener: {forceUpdated}} = this.props;

		if (forceUpdated) {
			this._input.current.select();
		}
	}

	render() {
		const {shortener: {isActiveForm, inputValue}} = this.props;

		return (
			<StyledWrapper>
				<StyledInput
					onChange={this.onChange}
					type="text"
					innerRef={this._input}
					value={inputValue}
					disabled={!isActiveForm}
					placeholder="Paste the link you want to shorten here"
				/>
				{this.getTooltip()}
			</StyledWrapper>
		);
	}
}

Input.propTypes = {
	changeValue: PropTypes.func.isRequired,
	shortener: PropTypes.shape({
		isActiveForm: PropTypes.bool.isRequired,
		inputValue: PropTypes.string.isRequired,
		status: PropTypes.string.isRequired,
		forceUpdated: PropTypes.bool
	})
};

export default Input;
