import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {shortener as shortenerActions} from '../../actions';

import Input from './Input';
import Submit from './Submit';


const Form = styled.form`
	display: flex;
	justify-content: space-between;
	margin-bottom: 74px;
`;

const InputWrapper = styled.div`
	flex-grow: 1;
`;

const ButtonWrapper = styled.div`
	width: 148px;
	margin-left: 15px;
`;

class Shortener extends React.Component {
	constructor(props) {
		super(props);
		this.changeValue = this.changeValue.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	changeValue(value) {
		const {store} = this.props;

		store.dispatch(shortenerActions.changeValue(value));
	}

	onSubmit(event) {
		event.preventDefault();
		const {shortenLink} = this.props;

		shortenLink();
	}

	render() {
		const {shortener} = this.props;

		return (
			<Form
				onSubmit={this.onSubmit}
			>
				<InputWrapper>
					<Input
						shortener={shortener}
						changeValue={this.changeValue}
					/>
				</InputWrapper>
				<ButtonWrapper>
					<Submit shortener={shortener} />
				</ButtonWrapper>
			</Form>
		);
	}
}

Shortener.propTypes = {
	store: PropTypes.object.isRequired,
	shortener: PropTypes.object.isRequired,
	shortenLink: PropTypes.func.isRequired
};

export default Shortener;
