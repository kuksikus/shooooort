const changeValue = (value, force) => ({
	type: 'CHANGE_VALUE',
	value,
	force
});

const inProcess = (isInProcess) => ({
	type: 'IN_PROCESS',
	inProcess: isInProcess
});

const changeStatus = (status, statusText) => ({
	type: 'STATUS',
	status,
	statusText
});

export default {
	changeValue,
	inProcess,
	changeStatus
};
