import axios from 'axios';

import shortener from './shortener';
import {copyToClipboard, getLinksFromStorage} from '../utils';
import {apiUrl} from '../api';

/**
 * Add link to storage
 * @param {Object} link Link object
 * @returns {void}
 */
const addLink = (link) => {
	const links = getLinksFromStorage();

	// @TODO проверка на существующую ссылку и нейминг

	localStorage.setItem('links', JSON.stringify([
		link,
		...links
	]));
};

const shortenSuccess = (link) => ({
	type: 'SHORTEN_SUCCESS',
	link
});

const clear = () => {
	localStorage.removeItem('links');

	return {
		type: 'CLEAR'
	};
};

const updateLinks = (links) => ({
	type: 'LINKS_UPDATE',
	links
});

/**
 * Check for already shortened links
 * @param {String} url URL to shorten
 * @returns {Object[] | undefined} Already shorten link
 */
const isLinkAlreadyShortened = (url) => {
	const links = getLinksFromStorage();

	return links.find((item) => item.url === url);
};

const sendLink = (url) => {
	return (dispatch) => {
		dispatch(shortener.inProcess(true));
		dispatch(shortener.changeStatus(''));

		const alreadyShortenLink = isLinkAlreadyShortened(url);

		if (alreadyShortenLink) {
			const text = 'This link already shortened';
			const shortenLink = `${apiUrl}/${alreadyShortenLink.code}`;

			dispatch(shortener.changeStatus('error', text));
			dispatch(shortener.inProcess(false));
			dispatch(shortener.changeValue(shortenLink, true));
			return Promise.resolve();
		}

		return axios
			.get(`/shorten?url=${encodeURIComponent(url)}`)
			.then((result) => {
				const {data: {code}} = result;
				const shortenLink = `${apiUrl}/${code}`;

				const data = {
					...result.data,
					url
				};

				addLink(data);
				copyToClipboard(shortenLink);

				dispatch(shortener.inProcess(false));
				dispatch(shortenSuccess(data));
				dispatch(shortener.changeStatus('success'));
				dispatch(shortener.changeValue(shortenLink, true));

			})
			.catch(() => {
				dispatch(shortener.inProcess(false));
				dispatch(shortener.changeStatus('error'));
			});
	};
};

const updateLinksInfo = (links) => {
	const updateLinksPromises = links.map(({code}) => {
		return code
			? axios.get(`/stats?shortcode=${code}`)
			: Promise.resolve();
	});

	return (dispatch) => {
		Promise
			.all(updateLinksPromises)
			.then((linksUpdate) => {
				if (!linksUpdate.length) {
					return;
				}

				const data = linksUpdate.map((item, index) => ({
					...links[index],
					...item.data
				}));

				localStorage.setItem('links', JSON.stringify(data));
				dispatch(updateLinks(data));
			})
			.catch(() => {
				return new Error('Update failed');
			});
	};
};

export default {
	sendLink,
	shortenSuccess,
	updateLinksInfo,
	clear
};
